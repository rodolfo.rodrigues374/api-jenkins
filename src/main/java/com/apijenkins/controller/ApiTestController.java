package com.apijenkins.controller;

import java.time.LocalDateTime;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value ="/api-test")
public class ApiTestController {

	@GetMapping
	public String get() {
		return "Novo Rodolfo Rodrigues " + LocalDateTime.now();
	}
}
